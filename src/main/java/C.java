import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class C {
    private static final String INPUT_FILE = "inputs/C-small-attempt0.in";
    private static final String OUTPUT_FILE = "outputs/C-small-attempt0.out";

    public static void main(String[] args) throws FileNotFoundException {
        InputStream inputStream = new FileInputStream(INPUT_FILE);
        Scanner scanner = new Scanner(inputStream);
        PrintWriter printWriter = new PrintWriter(new FileOutputStream(OUTPUT_FILE));

        int t = scanner.nextInt();
        assert t == 1;

        int n = scanner.nextInt();
        int j = scanner.nextInt();

        long startValue = raise2(n - 1) + 1;
        long endValue = raise2(n) - 1;

        int numberOfJamCoins = 0;

        printWriter.println("Case #1:");
out:    for (long value = startValue; value <= endValue && numberOfJamCoins < j; value += 2) {
            Integer[] array = toBinaryArr(value);

            List<Long> factors = new LinkedList<>();
            for (int base = 2; base <= 10; base++) {
                long interpretedValue = determineValue(array, base);
                long factor = getSmallestFactor(interpretedValue);
//                printWriter.println("interpretedValue:" + interpretedValue + ", factor:" + factor);
                if (factor == -1) {
                    continue out;
                } else {
                    factors.add(factor);
                }
            }

            numberOfJamCoins++;
            printWriter.println(getJamCoinString(array, factors));
        }

        printWriter.close();
    }

    static String getJamCoinString(Integer[] array, List<Long> factors) {
        StringBuilder sb = new StringBuilder();

        for (int digit : array) {
            sb.append(digit);
        }
        sb.append(" ");
        for (Long factor : factors) {
            sb.append(factor).append(" ");
        }

        return sb.toString();
    }

    static long determineValue(Integer[] array, int base) {
        long result = 0;

        for (Integer digit : array) {
            result = result * base + digit;
        }

        return result;
    }

    static long getSmallestFactor(long value) {
        for (int i = 2; i < Math.sqrt(value) + 1; i++) {
            if (value % i == 0) {
                return i;
            }
        }

        return -1;
    }

    static Integer[] toBinaryArr(long value) {
        List<Integer> binaryList = new LinkedList<>();
        while (value > 0) {
            binaryList.add((int) (value % 2));
            value /= 2;
        }
        Collections.reverse(binaryList);
        return binaryList.toArray(new Integer[binaryList.size()]);
    }

    static long raise2(int degree) {
        long result = 1;

        for (int i = 0; i < degree; i++) {
            result *= 2;
        }

        return result;
    }
}
