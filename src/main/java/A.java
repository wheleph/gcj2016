import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class A {
    private static final String INPUT_FILE = "inputs/A-large.in";
    private static final String OUTPUT_FILE = "outputs/A-large.out";

    public static void main(String[] args) throws FileNotFoundException {
        InputStream inputStream = new FileInputStream(INPUT_FILE);
        Scanner scanner = new Scanner(inputStream);
        PrintWriter printWriter = new PrintWriter(new FileOutputStream(OUTPUT_FILE));

        // Read input
        int n = scanner.nextInt();

        for (int i = 0; i < n; i++) {
            int k = scanner.nextInt();
            String answer = calculateAnswer(k);
            printWriter.println(String.format("Case #%d: %s", i + 1, answer));
        }

        printWriter.close();
    }

    private static String calculateAnswer(int k) {
        String answer = "INSOMNIA";
        if (k == 0) {
            return answer;
        }

        int i = 0;
        Set<Integer> digits = new HashSet<>();
        while (i <= 100 && digits.size() < 10) {
            i++;
            appendDigits(digits, i * k);
        }

        if (digits.size() == 10) {
            answer = String.valueOf(i * k);
        }

        return answer;
    }

    static void appendDigits(Set<Integer> digits, int k) {
        while (k > 0) {
            digits.add(k % 10);
            k /= 10;
        }
    }
}
