import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class B {
    private static final String INPUT_FILE = "inputs/B-large.in";
    private static final String OUTPUT_FILE = "outputs/B-large.out";

    public static void main(String[] args) throws FileNotFoundException {
        InputStream inputStream = new FileInputStream(INPUT_FILE);
        Scanner scanner = new Scanner(inputStream);
        PrintWriter printWriter = new PrintWriter(new FileOutputStream(OUTPUT_FILE));

        // Read input
        int n = scanner.nextInt();

        for (int i = 0; i < n; i++) {
            String input = scanner.next();
            int numberOfMoves = calculateNumberOfMoves(input);
            printWriter.println(String.format("Case #%d: %s", i + 1, numberOfMoves));
        }

        printWriter.close();
    }

    static int calculateNumberOfMoves(String input) {
        boolean[] inputArr = toArr(input);
        int depth = getDepth(inputArr);
        int numberOfMoves = 0;
        while (depth >= 0) {
            if (!inputArr[0]) {
                rotate(inputArr, depth);
            } else {
                int endIndex = 0;
                while (inputArr[endIndex + 1]) {
                    endIndex++;
                }

                rotate(inputArr, endIndex);
            }
            numberOfMoves++;
            depth = getDepth(inputArr);
        }
        return numberOfMoves;
    }

    private static void rotate(boolean[] inputArr, int depth) {
        for (int i = 0; i <= depth / 2; i++) {
            int j = depth - i;

            boolean tmp = inputArr[i];
            inputArr[i] = !inputArr[j];
            inputArr[j] = !tmp;
        }
    }

    private static int getDepth(boolean[] arr) {
        int i = arr.length - 1;
        for ( ; i >= 0; i--) {
            if (!arr[i]) {
                break;
            }
        }
        return i;
    }

    private static boolean[] toArr(String input) {
        boolean[] arr = new boolean[input.length()];

        for (int i = 0; i < input.length(); i++) {
            arr[i] = (input.charAt(i) == '+');
        }

        return arr;
    }

    private static String toStr(boolean[] arr) {
        StringBuilder sb = new StringBuilder();

        for (boolean b : arr) {
            sb.append(b ? "+" : "-");
        }

        return sb.toString();
    }
}
