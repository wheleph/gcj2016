import org.junit.Test;

import static org.junit.Assert.*;

public class CTest {
    @Test
    public void testRaise2() {
        assertEquals(1, C.raise2(0));
        assertEquals(2, C.raise2(1));
        assertEquals(4, C.raise2(2));
        assertEquals(8, C.raise2(3));
        assertEquals(16, C.raise2(4));
    }

    @Test
    public void testToBinaryArr() {
        assertArrayEquals(new Integer[] {1, 0, 1, 0}, C.toBinaryArr(10));
    }

    @Test
    public void testGetSmallestFactor() {
        assertEquals(-1, C.getSmallestFactor(5));
        assertEquals(-1, C.getSmallestFactor(13));

        assertEquals(2, C.getSmallestFactor(10));
        assertEquals(3, C.getSmallestFactor(9));
        assertEquals(11, C.getSmallestFactor(143));
    }

    @Test
    public void testDetermineValue() {
        assertEquals(10, C.determineValue(new Integer[] {1, 0, 1, 0}, 2));
        assertEquals(1010, C.determineValue(new Integer[] {1, 0, 1, 0}, 10));
    }
}