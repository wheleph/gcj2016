import org.junit.Assert;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

public class ATest {
    @Test
    public void testAppendDigits() {
        Set<Integer> digits = new HashSet<>();

        Set<Integer> expectedDigits = new HashSet<>();
        expectedDigits.add(1);
        expectedDigits.add(2);
        expectedDigits.add(3);
        expectedDigits.add(0);

        A.appendDigits(digits, 1203);

        Assert.assertEquals(expectedDigits, digits);
    }
}
