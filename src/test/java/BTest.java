import org.junit.Test;

import static org.junit.Assert.*;

public class BTest {
    @Test
    public void calculateNumberOfMoves() throws Exception {
        // Sample input
        assertEquals(1, B.calculateNumberOfMoves("-"));
        assertEquals(1, B.calculateNumberOfMoves("-+"));
        assertEquals(2, B.calculateNumberOfMoves("+-"));
        assertEquals(0, B.calculateNumberOfMoves("+++"));
        assertEquals(3, B.calculateNumberOfMoves("--+-"));

        // Additional input
        assertEquals(2, B.calculateNumberOfMoves("++-+"));
    }

}